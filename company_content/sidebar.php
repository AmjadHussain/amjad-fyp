 <?php
 require_once 'includes/config.php';

 
 $imagepath="http://localhost/php/jobsLocator/project/usercontent/media/mix/avatar.png";

$imagedata = mysqli_query($conn,"SELECT * from store WHERE UID={$_SESSION['UID']}");
if($imagedata){
  $xdata = mysqli_fetch_assoc($imagedata);
  $imagepath=$xdata['image'];
}
$user_detail_result = mysqli_query($conn,"SELECT * from user_details WHERE UID={$_SESSION['UID']}");
$userdetails= array();
if($user_detail_result){
  while(($row = mysqli_fetch_assoc($user_detail_result)) !=null){
    $userdetails[$row['option_key']]=$row['option_value'];
  }
}
?>









<div class="tab container sidebarportion "> 
 
   <div class="col-sm-3 tab_portion">
     <ul id="myTab" class="nav nav-tabs-vertical">
       <li class="active">
        <a href="#home" data-toggle="tab"> 
         <i class="glyphicon glyphicon-dashboard" > </i> 
           Dashboard
        </a>
      </li>
      <li class=" ">
        <a href="#profile" data-toggle="tab"> 
         <i class="glyphicon glyphicon-user" > </i> 
           Profile
        </a>
      </li>
      <li class=" ">
        <a href="#post_job" data-toggle="tab"> 
         <i class="glyphicon glyphicon-list" > </i> 
           Post a Job
        </a>
      </li>
      <li class=" ">
        <a href="#company_posts" data-toggle="tab"> 
         <i class="glyphicon glyphicon-list-alt" > </i> 
           Company Posts
        </a>
      </li>
          </ul>
  </div>   






  <div class="col-sm-8"> 
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade in active" id="home">
          <?php include_once  "tab_home.php"; ?>
        </div>
        <div class="tab-pane fade " id="profile">
          <?php include_once "tab_profile.php"; ?>
        </div>
        <div class="tab-pane fade " id="post_job">
          <?php include_once "vacancy_form.php"; ?>
        </div>
        <div class="tab-pane fade " id="company_posts">
          <?php include_once "manage_jobs.php"; ?>
        </div>
    </div>
  </div>

</div>