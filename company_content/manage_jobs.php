
<div class="row">
<div class="table-responsive">
<table id="table" class="table table-condensed table-stripped">
 <thead>
      <tr>
         <th>PID</th>
        <th>Title</th>
         <th>Salary</th>
         <th>View Applicants</th>
         <th>Update</th>
         <th>Information</th>
         <th>delete</th>
         
      </tr>

   </thead>

  <tbody>
<?php 
// print_r($_SESSION);
$id = $_SESSION['details']['UID'];
$query="SELECT * FROM vacancy WHERE UID ={$id}";
$rslt=mysqli_query($conn,$query);
while ($data=mysqli_fetch_assoc($rslt)) {
   $pid=$data['PID'];
   $title=$data['title'];
   $salary=$data['salary'];
   
 ?>
      <tr class="active">
         <td><?php echo $pid; ?></td>
         <td><?php echo $title; ?></td>
         <td><?php echo $salary; ?></td>
         <td><a href="<?php echo $BASE_URL; ?>?page=viewapplicants&PID=<?php echo $data['PID']; ?>"><button class="btn btn-primary"><i class ="glyphicon glyphicon-check"></i> Applied Applicants</button></td>
         <td><a href="<?php echo $BASE_URL; ?>?page=update&JID=<?php echo $data['PID']; ?>" class=" pull-right btn btn-success"><i class ="glyphicon glyphicon-edit"></i> Update</td>
         <td><a href="<?php echo $BASE_URL; ?>?page=viewvacancy&UID=<?php echo $data['PID']; ?>"><button class="btn btn-info"><i class ="glyphicon glyphicon-eye-open"></i> View Info</button></td>
         <td><a href="<?php echo $BASE_URL; ?>/actions/delete_job_action.php?PID=<?php echo $data['PID']; ?>"><button class="btn btn-danger"><i class ="glyphicon glyphicon-trash"></i> delete</button></td>
         
      </tr>
      <?php } ?>
   </tbody>
</table>
</div>
</div>

<script>
window.addEventListener('load',function(){
   
    $('#company').DataTable();
});
</script>