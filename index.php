<?php 	require_once 'includes/config.php';
if (isset($_GET['message'])) {
	//echo "<b style='color:red; margin-top:200px'>".str_replace("+", " ", $_GET['message'])." </b>";
	# code...
}
// $session_id = $_SESSION['UID'];
// echo $session_id;
   // print_r($_SESSION);    

$HEADER ='header/static.php';
$CONTENT ='content/main_content.php';
$FOOTER ='footer/static.php';

if(isset($_GET['page']) && $_GET['page'] == "home"){
	$CONTENT= 'content/main_content.php';
}
elseif (isset($_GET['page']) && $_GET['page']=="contact" ) {
	$CONTENT= 'content/contact.php';
}
elseif (isset($_GET['page']) && $_GET['page'] == "signup") {
	$CONTENT ='content/Signup.php';
}
elseif (isset($_GET['page']) && $_GET['page'] == "search") {
	 $CONTENT ='content/search.php';
}
else if(isset($_GET['page']) && $_GET['page'] == "login"){
	
	$CONTENT= 'content/login.php';
	
	if  (isset($_SESSION['isLoggedIn']) && $_SESSION['details']["profile"]=='Admin' ) {
		header("location:index.php?page=admindashboard");
	}
	else if  (isset($_SESSION['isLoggedIn']) && $_SESSION['details']["profile"]=='Job_provider' ) {
		header("location:index.php?page=companydashboard");
	}
	else if  (isset($_SESSION['isLoggedIn']) && $_SESSION['details']["profile"]=='Job_seeker' ) {
		header("location:index.php?page=dashboard");
	}

  }
	else if(isset($_GET['page']) && $_GET['page'] == "dashboard"){

	//check auth if false then reirect to login page.
	if(!isset($_SESSION['isLoggedIn'])){
		header("location:index.php?message=invalid+login+details");
	}

	$HEADER='header/dashboard_header.php';
	$CONTENT= 'usercontent/dashboard.php';

}else if(isset($_GET['page']) && $_GET['page'] == "companydashboard"){

	//check auth if false then reirect to login page.
	if(!isset($_SESSION['isLoggedIn'])){
		header("location:index.php?message=invalid+login+details");
	}

	$HEADER='header/company_header.php';
	$CONTENT= 'company_content/company_dashboard.php';

}
else if(isset($_GET['page']) && $_GET['page'] == "admindashboard"){

	//check auth if false then reirect to login page.
	if(!isset($_SESSION['isLoggedIn'])){
		header("location:index.php?message=invalid+login+details");
	}

	$HEADER='header/admin_dashboard.php';
	$CONTENT= 'admin_content/admin_dashboard.php';

}
else if(isset($_GET['page']) && $_GET['page'] == "publicprofile"){

	if(isset($_GET['cid'])){
		$CONTENT= 'content/user_profile.php';
	}
	//handle user profile preview here just like abouve
	
}else if(isset($_GET['page']) && $_GET['page'] == "viewjob"){

	$CONTENT= 'content/view_job.php';

}else if(isset($_GET['page']) && $_GET['page'] == "viewvacancy"){

	$CONTENT= 'company_content/view_job.php';

}

else if(isset($_GET['page']) && $_GET['page'] == "viewapplicants"){
	if(!isset($_SESSION['isLoggedIn'])){
		header("location:index.php?message=invalid+login+details");
	}

	$CONTENT= 'company_content/view_applicants.php';

}else if(isset($_GET['page']) && $_GET['page'] == "update"){

	$CONTENT= 'company_content/update_job.php';

}else if(isset($_GET['page']) && $_GET['page'] == "update_details"){

	
	
    $HEADER='header/admin_dashboard.php';
	$CONTENT= 'admin_content/update_info.php';

}
else if(isset($_GET['page']) && $_GET['page'] == "userinfo"){

	
	
    $HEADER='header/admin_dashboard.php';
	$CONTENT= 'admin_content/users_info.php';

}
else if (isset($_GET['page']) && $_GET['page'] == "logout") {
require_once 'logout.php';
}

else if (isset($_GET['page']) && $_GET['page'] == "search") {
require_once 'content/search.php';
}


require_once $HEADER;
$containers=explode("#", $CONTENT);
foreach ($containers as $container){
	require_once trim($container);
}
require_once $FOOTER; 
 ?>

