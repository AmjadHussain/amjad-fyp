-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2017 at 03:10 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jbl`
--

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

CREATE TABLE `cv` (
  `CVID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cv` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cv`
--

INSERT INTO `cv` (`CVID`, `UID`, `name`, `cv`) VALUES
(1, 0, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/mix/avatar.png'),
(2, 3, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/cv/amjad.pdf'),
(3, 85, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/cv/naveed Abbass.pdf'),
(4, 41, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/cv/Ali Ahmed Jan.pdf'),
(5, 94, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/cv/Noreen.docx'),
(6, 96, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/cv/Amjad Hussain.docx');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE `job_post` (
  `PID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `category` enum('Govt','Private','Full_Time','Part_Time','NGO') NOT NULL,
  `dates` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`PID`, `UID`, `title`, `description`, `category`, `dates`) VALUES
(4, 79, 'Instructtor for Diploma in IT', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Govt', '0000-00-00 00:00:00'),
(5, 79, 'Web Designer', 'Web and Graphics Designer instructor needs in NEVTAC, Interesting candidated may apply before 5 July 2k17.', 'Private', '0000-00-00 00:00:00'),
(6, 80, 'Managing Director ', 'Managing Director post vaccant in Natco Corporation Gilgit, Interested candidates may apply before 22 june of this month, Candidate should  have domicile of Gilgit Baltistan.', 'Govt', '0000-00-00 00:00:00'),
(7, 86, 'Marketing Manager', 'marketing Manager post vacant at Pak Trader Gilgit, Applicants having pakistani identity may apply for this post.\r\nNeeds: Master Degeree in Management Sciences\r\nExperience : 3 years in related field\r\n', 'Private', '0000-00-00 00:00:00'),
(8, 86, 'Data Entry Operator', 'Data Entry operator needs in Pak Trader gilgit , Minimum qualification F.A, FSC,, Interested candidates may apply for this job. Post your as soon as possible , last date to apply, 15,April 2k17.', 'Private', '0000-00-00 00:00:00'),
(9, 86, 'Data Entry Operator', 'Data Entry operator needs in Pak Trader gilgit , Minimum qualification F.A, FSC,, Interested candidates may apply for this job. Post your as soon as possible , last date to apply, 15,April 2k17.', 'Private', '0000-00-00 00:00:00'),
(10, 86, 'Data Entry Operator', 'Data Entry operator needs in Pak Trader gilgit , Minimum qualification F.A, FSC,, Interested candidates may apply for this job. Post your as soon as possible , last date to apply, 15,April 2k17.', 'Private', '0000-00-00 00:00:00'),
(11, 86, 'Data Entry Operator', 'Data Entry operator needs in Pak Trader gilgit , Minimum qualification F.A, FSC,, Interested candidates may apply for this job. Post your as soon as possible , last date to apply, 15,April 2k17.', 'Private', '0000-00-00 00:00:00'),
(12, 86, 'Data Entry Operator', 'Data Entry operator needs in Pak Trader gilgit , Minimum qualification F.A, FSC,, Interested candidates may apply for this job. Post your as soon as possible , last date to apply, 15,April 2k17.', 'Private', '0000-00-00 00:00:00'),
(14, 86, 'Professor', 'this is inform to all the willing condidates to  submit their CVS.', 'Govt', '0000-00-00 00:00:00'),
(15, 86, 'Lecturer', 'sfdfggfdfg dfgdf gdfggdtf gdfg', 'Govt', '0000-00-00 00:00:00'),
(16, 86, 'Lecturer', 'hgfh  fgd dfgd fgdf dfgdf dfgdfg', 'Govt', '2017-04-12 00:00:00'),
(17, 79, 'Java Developer', 'fdjf dk  dfjdjfkdj  ddfjd df dkfjkdj f d fd ddfddd', 'Private', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `myapplications`
--

CREATE TABLE `myapplications` (
  `MAID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `PID` bigint(20) NOT NULL,
  `appliedon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `myapplications`
--

INSERT INTO `myapplications` (`MAID`, `UID`, `PID`, `appliedon`, `status`) VALUES
(3, 94, 58, '2017-04-17 18:50:55', 0),
(4, 96, 17, '2017-04-17 19:46:36', 0),
(5, 96, 59, '2017-04-17 19:47:49', 0),
(6, 79, 80, '2017-04-17 22:42:07', 0),
(7, 96, 80, '2017-04-18 06:41:55', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post_action`
--

CREATE TABLE `post_action` (
  `PAID` bigint(20) NOT NULL,
  `PID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `save_job` bigint(50) NOT NULL,
  `apply_job` varchar(50) NOT NULL,
  `upload_cv` int(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `SID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `setting_key` varchar(30) NOT NULL,
  `setting_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `UID` bigint(20) NOT NULL,
  `SID` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`UID`, `SID`, `title`, `slug`, `status`) VALUES
(0, 1, 'Android', 'android', 0),
(0, 2, 'PHP Developer', 'php_developer', 0),
(0, 3, 'Database', 'database', 0),
(0, 4, 'Teacher', 'teacher', 0),
(0, 5, 'Data Entry Opearator', 'data_entry_opearator', 0),
(0, 6, 'Clerk', 'clerk', 0),
(0, 7, 'Article Writer', 'article_writer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `UID` bigint(20) NOT NULL,
  `imgID` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`UID`, `imgID`, `name`, `image`) VALUES
(44, 13, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/profilepics/amjad2.jpg'),
(50, 15, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/Soneri Bank Danyore.png'),
(75, 19, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/Alfalah Bank .png'),
(76, 20, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/K2 Traders.png'),
(79, 23, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/NEVTAC Gilgit.png'),
(80, 24, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/Natco Corporation Gilgit.png'),
(84, 25, 'profile pic', 'http://localhost/php/jobsLocator/project/admin_content/media/profilepics/amjad002.png'),
(86, 27, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/Pak Trader Gilgit.png'),
(87, 28, 'profile pic', 'http://localhost/php/jobsLocator/project/company_content/media/profilepics/Hashoo Foundation.png'),
(94, 31, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/profilepics/Noreen.jpg'),
(96, 32, 'profile pic', 'http://localhost/php/jobsLocator/project/usercontent/media/profilepics/Amjad Hussain.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UID` bigint(20) NOT NULL,
  `profile` enum('Admin','Job_seeker','Job_provider') NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(20) NOT NULL,
  `phone` bigint(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UID`, `profile`, `username`, `password`, `email`, `phone`) VALUES
(44, 'Job_provider', 'Kado', 'kado123', 'kado@gmail.com', 581145235),
(46, 'Job_provider', 'Allied Bank Danyore', 'allied123', 'allied@gmail.com', 581142563),
(48, 'Job_provider', 'itbrowser ', 'itbrowser123', 'itbrowserpk@gmail.co', 58116535),
(50, 'Job_provider', 'Soneri Bank Danyore', 'soneri', 'soneri@gmail.com', 5811452555),
(51, 'Job_provider', 'Hashwaan Traders', 'hashwaan123', 'hashwaan@gmail.com', 5811455662),
(58, 'Job_provider', 'karakoram Traders', 'kiu123', 'kara@gmail.com', 5245555865),
(61, 'Job_provider', 'National Bakers', 'national123', 'national123@gmail.co', 126555666),
(72, 'Job_provider', 'Sehat Foundation Danyore', 'sehat123', 'sehat@gmail.com', 581125555),
(73, 'Job_provider', 'APS', 'aps123', 'aps@gmail.com', 581145255),
(75, 'Job_provider', 'Alfalah Bank ', 'alfalah123', 'alfalah@gmail.com', 5811233333),
(76, 'Job_provider', 'K2 Traders', 'k2123', 'k2@gmail.com', 581122552222),
(77, 'Job_provider', 'Cooperative Bank', 'coop123', 'coop@gmail.com', 581154552),
(79, 'Job_provider', 'NEVTAC Gilgit', 'nevtac123', 'nectac@gmail.com', 581145855),
(80, 'Job_provider', 'Natco Corporation Gilgit', 'natco123', 'natco@gmail.com', 5811458855),
(84, 'Admin', 'amjad002', 'amjad002', 'amj@999gmail.com', 215445422221),
(86, 'Job_provider', 'Pak Trader Gilgit', 'pak123', 'pak@gmail.com', 45654511222),
(87, 'Job_provider', 'Hashoo Foundation', 'hashoo123', 'hashoo33@gmail.com', 581145245),
(94, 'Job_seeker', 'Noreen', 'noreen123', 'noreen@gmail.com', 31245855545),
(96, 'Job_seeker', 'Amjad Hussain', 'amjad525', 'amju525@gmail.com', 3448840525),
(97, 'Admin', 'Jaan', 'jaan123', 'jaan123@gmail.com', 344855555555),
(98, 'Admin', 'fghfghfg', 'fghfghfghfg', 'aherterter@gmail.com', 3425177351),
(99, 'Admin', 'waseemoooooooooooooo', 'wasww8999', 'waseem22222222222222', 12555555555441111);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `UDID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `option_key` varchar(50) NOT NULL,
  `option_value` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`UDID`, `UID`, `option_key`, `option_value`, `status`) VALUES
(307, 44, 'address', '', 0),
(308, 44, 'gender', '', 0),
(309, 44, 'dob', '', 0),
(310, 44, 'country', '', 0),
(311, 44, 'experience', '', 0),
(312, 44, 'qualification', '', 0),
(313, 46, 'address', '', 0),
(314, 46, 'gender', '', 0),
(315, 46, 'dob', '', 0),
(316, 46, 'country', '', 0),
(317, 46, 'experience', '', 0),
(318, 46, 'qualification', '', 0),
(325, 48, 'address', '', 0),
(326, 48, 'gender', '', 0),
(327, 48, 'dob', '', 0),
(328, 48, 'country', '', 0),
(329, 48, 'experience', '', 0),
(330, 48, 'qualification', '', 0),
(337, 50, 'address', '', 0),
(338, 50, 'gender', '', 0),
(339, 50, 'dob', '', 0),
(340, 50, 'country', '', 0),
(341, 50, 'experience', '', 0),
(342, 50, 'qualification', '', 0),
(343, 51, 'address', '', 0),
(344, 51, 'gender', '', 0),
(345, 51, 'dob', '', 0),
(346, 51, 'country', '', 0),
(347, 51, 'experience', '', 0),
(348, 51, 'qualification', '', 0),
(388, 72, 'website', '', 0),
(389, 72, 'organization', '', 0),
(390, 72, 'establisment', '', 0),
(391, 73, 'website', '', 0),
(392, 73, 'organization', '', 0),
(393, 73, 'establisment', '', 0),
(397, 75, 'website', 'www/a;falahbank.com', 0),
(398, 75, 'organization', 'Banking', 0),
(399, 75, 'country', 'Pakistan', 0),
(400, 75, 'location', 'chandni chowk danyore , gilgit', 0),
(401, 75, 'establishment', '2017-03-16', 0),
(402, 76, 'website', 'ffffffffff', 0),
(403, 76, 'organization', 'hhhhhhhh', 0),
(404, 76, 'country', 'ffffffffffffffffff', 0),
(405, 76, 'location', 'jhhh', 0),
(406, 76, 'establishment', '2017-03-24', 0),
(407, 77, 'website', 'www.cooperativebanklmt.com', 0),
(408, 77, 'organization', 'banking', 0),
(417, 79, 'website', 'www.prof.nevtac.pk', 0),
(418, 79, 'organization', 'Govt', 0),
(419, 79, 'country', 'Pakistan', 0),
(420, 79, 'location', 'gilgit', 0),
(421, 79, 'establishment', '2017-03-17', 0),
(422, 80, 'website', 'www.natco.org.pk', 0),
(423, 80, 'organization', 'Transportation', 0),
(424, 80, 'country', 'pakistan', 0),
(425, 80, 'location', 'Gilgit', 0),
(426, 80, 'establishment', '2017-03-21', 0),
(427, 84, 'nic', '7150106201149', 0),
(428, 84, 'address', 'chandni chowk danyore gilgit', 0),
(429, 84, 'country', 'china', 0),
(430, 84, 'qualification', 'BSC', 0),
(431, 84, 'destination', 'Content Manager', 0),
(438, 86, 'website', 'www.paktrader.pk', 0),
(439, 86, 'organization', 'Bussiness', 0),
(440, 86, 'country', 'Pakistan', 0),
(441, 86, 'location', 'Gigit Baltistan', 0),
(442, 86, 'establishment', '2017-04-20', 0),
(443, 87, 'website', 'www.hashoo.gov.pk', 0),
(444, 87, 'organization', 'Training Development', 0),
(445, 87, 'country', 'Pakistan', 0),
(446, 87, 'location', 'Gilgit', 0),
(447, 87, 'establishment', '2017-04-21', 0),
(461, 94, 'address', 'syed abad danyore gilgit', 0),
(462, 94, 'gender', 'female', 0),
(463, 94, 'dob', '2017-04-13', 0),
(464, 94, 'city', 'Gilgit', 0),
(465, 94, 'country', 'pakistan', 0),
(466, 94, 'experience', '1y', 0),
(467, 94, 'skills', 'Accounting, Computer skills', 0),
(468, 94, 'xfield', 'Accounting & Finance', 0),
(469, 94, 'qualification', 'bs', 0),
(470, 96, 'address', 'Chandni chowk , Syed-abad Danyore Gilgit', 0),
(471, 96, 'gender', 'male', 0),
(472, 96, 'dob', '2017-01-12', 0),
(473, 96, 'city', 'Gilgit', 0),
(474, 96, 'country', 'Pakistan', 0),
(475, 96, 'experience', '1y', 0),
(476, 96, 'skills', 'Web and Graphics Designer, PHD Web Developer', 0),
(477, 96, 'xfield', 'Information Technology', 0),
(478, 96, 'qualification', 'phd', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE `vacancy` (
  `PID` bigint(20) NOT NULL,
  `UID` bigint(20) NOT NULL,
  `title` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `qualification` varchar(512) NOT NULL,
  `xfield` varchar(512) NOT NULL,
  `experience` varchar(512) NOT NULL,
  `required_number` varchar(512) DEFAULT NULL,
  `additional_req` varchar(512) DEFAULT NULL,
  `salary` varchar(512) DEFAULT NULL,
  `job_location` varchar(512) DEFAULT NULL,
  `skills` text NOT NULL,
  `benefits` varchar(512) DEFAULT NULL,
  `employment_type` varchar(512) DEFAULT NULL,
  `contract_duration` varchar(512) DEFAULT NULL,
  `post_date` varchar(512) DEFAULT NULL,
  `dead_line` varchar(512) DEFAULT NULL,
  `department` varchar(512) DEFAULT NULL,
  `location` varchar(512) DEFAULT NULL,
  `phone_number` varchar(512) DEFAULT NULL,
  `po_box` varchar(512) DEFAULT NULL,
  `email` varchar(512) DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `how_to_apply` varchar(512) DEFAULT NULL,
  `source` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`PID`, `UID`, `title`, `description`, `qualification`, `xfield`, `experience`, `required_number`, `additional_req`, `salary`, `job_location`, `skills`, `benefits`, `employment_type`, `contract_duration`, `post_date`, `dead_line`, `department`, `location`, `phone_number`, `po_box`, `email`, `city`, `how_to_apply`, `source`) VALUES
(17, 79, 'Teacher nneeededxx', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(25, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(26, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(52, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(55, 80, 'Teacher required', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(56, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(57, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(58, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(59, 80, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(79, 79, 'Teacher', '\n                    ', '', 'Driver', 'Range: , Required: ', '6', 'Driving licences from govt organization', 'Negotiable', 'Addis Ababa', '', '', 'Permanent', NULL, '17/04/17', '17/04/17', '', '', '', '', '', 'Addis Ababa', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. In Person', ''),
(80, 79, 'HR Manager', 'Applications are invited for required post, Applicant having domicile of Gilgit Baltistn may apply for this jobs. Applications are invited for required post, Applicant having domicile of Gilgit Baltistn may apply for this jobs.Applications are invited for required post, Applicant having domicile of Gilgit Baltistn may apply for this jobs.Applications are invited for required post, Applicant having domicile of Gilgit Baltistn may apply for this jobs.Applications are invited for required post, Applicant having domicile of Gilgit Baltistn may apply for this jobs.', 'MBA, BBA', 'Business Administration/Management', 'Range: 3-4 years, Required: 1 year', '1', 'Minimum 1 year experience from any organization in related field', '6000000 PKR', 'Ghizer', 'Managerial Skills, Dealing Skills', 'Health and Residents facilities', 'Permanent', NULL, '17/04/17', '17/04/17', 'Account and Finace', 'gilgit', '581145855', 'p.o.box street No:11 ghizer', 'nectac@gmail.com', 'Ghizer', 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. Through Email', 'Reporter');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`CVID`),
  ADD KEY `UID` (`UID`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
  ADD PRIMARY KEY (`PID`),
  ADD KEY `UID` (`UID`);

--
-- Indexes for table `myapplications`
--
ALTER TABLE `myapplications`
  ADD PRIMARY KEY (`MAID`),
  ADD KEY `UID` (`UID`),
  ADD KEY `PID` (`PID`);

--
-- Indexes for table `post_action`
--
ALTER TABLE `post_action`
  ADD PRIMARY KEY (`PAID`),
  ADD KEY `PID` (`PID`),
  ADD KEY `UID` (`UID`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`SID`),
  ADD KEY `UID` (`UID`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`SID`),
  ADD KEY `UID` (`UID`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`imgID`),
  ADD KEY `UID` (`UID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`UDID`),
  ADD KEY `UID` (`UID`),
  ADD KEY `UID_2` (`UID`),
  ADD KEY `UID_3` (`UID`);

--
-- Indexes for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`PID`),
  ADD KEY `UID` (`UID`),
  ADD KEY `UID_2` (`UID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv`
--
ALTER TABLE `cv`
  MODIFY `CVID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
  MODIFY `PID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `myapplications`
--
ALTER TABLE `myapplications`
  MODIFY `MAID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `post_action`
--
ALTER TABLE `post_action`
  MODIFY `PAID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `SID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `SID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `imgID` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `UDID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=479;
--
-- AUTO_INCREMENT for table `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `PID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_post`
--
ALTER TABLE `job_post`
  ADD CONSTRAINT `job_post_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`);

--
-- Constraints for table `myapplications`
--
ALTER TABLE `myapplications`
  ADD CONSTRAINT `myapplications_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `myapplications_ibfk_2` FOREIGN KEY (`PID`) REFERENCES `vacancy` (`PID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `post_action`
--
ALTER TABLE `post_action`
  ADD CONSTRAINT `post_action_ibfk_1` FOREIGN KEY (`PID`) REFERENCES `job_post` (`PID`),
  ADD CONSTRAINT `post_action_ibfk_2` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`);

--
-- Constraints for table `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_details_ibfk_2` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD CONSTRAINT `vacancy_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
