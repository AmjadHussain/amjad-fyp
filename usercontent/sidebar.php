<?php 
require_once 'includes/config.php';
// require_once 'header/common.php'; ?>


<!-- <pre>
<?php
// print_r($_SESSION);

?>
</pre -->
<div class="tab container sidebarportion"> 
 
   <div class="col-sm-3 sidebar_style">
     <ul id="myTab" class="nav nav-tabs-vertical">
      <li class="active">
        <a href="#home" data-toggle="tab"> 
         <i class="glyphicon glyphicon-edit" > </i> 
           Edit Profile
        </a>
      </li>
      <li class=" ">
        <a href="#profile" data-toggle="tab"> 
         <i class="glyphicon glyphicon-user" > </i> 
           Profile
        </a>
      </li>
       
      <li class=" ">
        <a href="#cv_post" data-toggle="tab"> 
         <i class="glyphicon glyphicon-upload" > </i> 
           Upload your CV
        </a>
      </li>
      <li class=" ">
        <a href="#myapplications" data-toggle="tab"> 
         <i class="glyphicon glyphicon-saved" > </i> 
           My Applications
        </a>
      </li>
      
          </ul>
  </div>   
 <!-- tab host -->


 <?php

 $cvpath="http://localhost/php/jobsLocator/project/usercontent/media/mix/demo.jpg";
//query fix
$cvdata = mysqli_query($conn,"SELECT * from cv WHERE UID={$_SESSION['UID']}");
if($cvdata){
  $cvresult = mysqli_fetch_assoc($cvdata);
  $cvpath=$cvresult['cv'];
}

 $imagepath="http://localhost/php/jobsLocator/project/usercontent/media/mix/avatar.png";

$imagedata = mysqli_query($conn,"SELECT * from store WHERE UID={$_SESSION['UID']}");
if($imagedata){
  $xdata = mysqli_fetch_assoc($imagedata);
  $imagepath=$xdata['image'];
}

$user_detail_result = mysqli_query($conn,"SELECT * from user_details WHERE UID={$_SESSION['UID']}");
$userdetails= array();
if($user_detail_result){
  while(($row = mysqli_fetch_assoc($user_detail_result)) !=null){
    $userdetails[$row['option_key']]=$row['option_value'];
  }
}
 ?>
  <div class="col-sm-8"> 
    <div id="myTabContent" class="tab-content">
        
        <div class="tab-pane fade in active" id="home">
          <?php include_once  "tab_home.php"; ?>
        </div>
        <div class="tab-pane fade " id="profile">
          <?php include_once "tab_profile.php"; ?>
        </div>
        <div class="tab-pane fade " id="cv_post">
          <?php include_once "cv_post.php"; ?>
        </div>
        <div class="tab-pane fade " id="myapplications">
          <?php include_once "myapplications.php"; ?>
        </div>
    </div>
  </div>

</div>
<script  type="text/javascript" src="<?php echo $BASE_URL ; ?>/js/common.js"></script>


