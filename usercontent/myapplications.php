<?php


$status=array('Applied','Viewed By Employer','Rejected','Accepted');

$UID = $_SESSION['details']['UID'];
$application_data = mysqli_query($conn,"SELECT A.appliedon,A.status,J.title,J.PID from myapplications as A,vacancy as J WHERE A.PID=J.PID AND A.UID={$UID}");
?>
<div class="container-fluid">
<div>
	<h1>Job Applications</h1>
</div>
	<div class="table-responsive">
		<table id="users" class="table table-bordered table-stripped">
			<thead>
				<tr>
					<th>S#</th>
					<th>Job Title</th>
					<th>Applied On</th>
					<th>Status</th>
					<th>View</th>
					<th>Response</th>
				</tr>

			</thead>
			<tbody>
			<?php
			$i=1;
			if($application_data):
				while(($row = mysqli_fetch_assoc($application_data))!=null):
			?>
				<tr>
					<td>
						<?php echo $i; ?>
					</td>
					<td>
						<?php echo $row['title']; ?>
					</td>
					<td>
						<?php echo $row['appliedon']; ?>
					</td>
					<td>
						<?php echo $status[$row['status']]; ?>
					</td>
					<td>
						<a href="<?php echo $BASE_URL,"/index.php?page=viewjob&JID=",$row['PID']; ?>" target="blank">
							View
						</a>
						
					</td>
					<td>  null	</td>
				</tr>
			<?php
				$i++;
				endwhile;
			else:
				echo "<tr><td colspan='5'>Sorry you have not applied for any job</td><tr>";
			endif;
			?>
			</tbody>
		</table>
	</div>
</div>
<script>
window.addEventListener('load',function(){
	
    $('#users').DataTable();
});
</script>