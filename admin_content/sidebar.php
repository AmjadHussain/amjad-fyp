 <?php
 require_once 'includes/config.php';

 
 $imagepath="http://localhost/php/jobsLocator/project/usercontent/media/mix/avatar.png";

$imagedata = mysqli_query($conn,"SELECT * from store WHERE UID={$_SESSION['UID']}");
if($imagedata){
  $xdata = mysqli_fetch_assoc($imagedata);
  $imagepath=$xdata['image'];
}
$user_detail_result = mysqli_query($conn,"SELECT * from user_details WHERE UID={$_SESSION['UID']}");
$userdetails= array();
if($user_detail_result){
  while(($row = mysqli_fetch_assoc($user_detail_result)) !=null){
    $userdetails[$row['option_key']]=$row['option_value'];
  }
}
?>


   

<div class="tab container sidebarportion"> 

   <div class="col-sm-3 tab_portion">
     <ul id="myTab" class="nav nav-tabs-vertical">
       <li class="active">
        <a href="#home" data-toggle="tab"> 
         <i class="glyphicon glyphicon-dashboard" > </i> 
           Create Admin
        </a>
      </li>
      <li class=" ">
        <a href="#profile" data-toggle="tab"> 
         <i class="glyphicon glyphicon-user" > </i> 
           Profile
        </a>
      </li>
      
      <li class=" ">
        <a href="#seekers_details" data-toggle="tab"> 
         <i class="glyphicon glyphicon-adjust" > </i> 
           Manage Users
        </a>
      </li>
      <li class=" ">
        <a href="#admin_post" data-toggle="tab"> 
         <i class="glyphicon glyphicon-edit" > </i> 
           Post a job
        </a>
      </li>
          </ul>
  </div>   






  <div class="col-sm-8"> 
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade in active" id="home">
          <?php include_once  "tab_create_admin.php"; ?>
        </div>
        <div class="tab-pane fade " id="profile">
          <?php include_once "tab_profile.php"; ?>
        </div>
        
        <div class="tab-pane fade " id="seekers_details">
          <?php include_once "tab_seekers.php"; ?>
        </div>
        <div class="tab-pane fade " id="admin_post">
          <?php include_once "admin_post.php"; ?>
        </div>
    </div>
  </div>

</div>