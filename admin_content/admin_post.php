<?php  ?>
<div class="container-fluid" id="divAddVacancy">
    <h2><span class="fa fa-send"></span> Add Vacancy</h2>
    <h3>Vacancy information</h3>
    <form role="form" id="myform">
        <div class="row">
            <span class="col-md-4">
                <strong>Title</strong>
            </span>
            <span class="col-md-4">
                <strong>Qualification</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-4">
                    <input type="text" class="form-control" id="txtTitle">
            </span>
            <span class="col-md-4">
                 <div class="tokenfield form-control" style="width: 100%;"><input type="text" class="form-control" id="txtQualification" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" class="token-input ui-autocomplete-input" autocomplete="off" placeholder="" id="txtQualification-tokenfield" tabindex="0" style="min-width: 60px; width: 321px;"></div>
            </span>
        </div>
        <!-- input row 2 -->
        <div class="row">
            <span class="col-md-3">
                <strong>Field</strong>
            </span>
            <span class="col-md-3">
                <strong>Experience range</strong>
            </span>
			<span class="col-md-3">
                <strong>Experience required</strong>
            </span>
             <span class="col-md-3">
                <strong>Required Number</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-3">
                  <div class="tokenfield form-control" style="width: 100%;"><input type="text" class="form-control" id="txtField" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" class="token-input ui-autocomplete-input" autocomplete="off" placeholder="" id="txtField-tokenfield" tabindex="0" style="min-width: 60px; width: 224px;"></div>
            </span>
            <span class="col-md-3">
                 <div class="tokenfield form-control" style="width: 100%;"><input type="text" class="form-control" id="txtExperience" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" class="token-input ui-autocomplete-input" autocomplete="off" placeholder="" id="txtExperience-tokenfield" tabindex="0" style="min-width: 60px; width: 224px;"></div>
            </span>
            <span class="col-md-3">
                 <input type="text" class="form-control" id="txtExperienceRequired">
            </span>
             <span class="col-md-3">
                <input type="text" class="form-control" id="txtNumber">
            </span>
        </div>
        <!-- input for row 3: Addition requirements-->
        <div class="row">
            <span class="col-md-12">
               <strong>Additional Requirements</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-12">
                <input type="text" id="txtAdditionReq" class="form-control">
            </span>
        </div>
        <!-- input for row 4 : 6 columns -->
        <div class="row">
            <span class="col-md-2">
               <strong>Salary</strong>
            </span>
            <span class="col-md-6">
            </span>
             <span class="col-md-4">
               <strong>Job Location</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-2">
                <input type="text" id="txtSalary" class="form-control" disabled="">
            </span>
             <span class="col-md-6">
                 <div class="row">
                     <label class="radio-inline col-sm-3"><input type="radio" name="opSalary" value="Negotiable" onclick="onSalary(&#39;Negotiable&#39;);" checked="checked">Negotiable</label>
                     <label class="radio-inline col-sm-3"><input type="checkbox" name="opSalaryAttractive">Attractive</label>
                     <label class="radio-inline col-sm-3"><input type="radio" name="opSalary" value="Percompany Scale" onclick="onSalary(&#39;Percompany Scale&#39;);">Percompany
                         Scale</label>
                     <label class="radio-inline"><input type="radio" name="opSalary" value="Fixed" onclick="onSalary(&#39;fixed&#39;);">Fixed</label>
                 </div>
            </span>
             <span class="col-md-4">
                <div class="tokenfield form-control" style="width: 100%;">
                    <input type="text" id="txtJobLocation" class="form-control" value="Gilgit" tabindex="-1" style="position: absolute; left: -10000px;">
                    <input type="text" tabindex="-1" style="position: absolute; left: -10000px;">
                    <input type="text" class="token-input ui-autocomplete-input" autocomplete="off" placeholder="" id="txtJobLocation-tokenfield" tabindex="0" style="min-width: 60px; width: 126px;">
                </div>
            </span>
        </div>

        <div class="form-group">
        <label>Skills:</label>
           
            <!-- <label>Job Description</label> -->
            <textarea class="form-control" rows="5" cols="50" id="txtskills">

            </textarea>
             
        </div>
        <!-- input row 5: Benefits -->
        <div class="row">
            <span class="col-md-12">
               <strong>Benefits</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-12">
                <input type="text" id="txtBenifits" class="form-control">
            </span>
        </div>
        <!-- input row 6: 4 rows -->
        <div class="row">
            <span class="col-md-3">
               <strong>Employment type</strong>
            </span>
              <span class="col-md-3">
               <strong>Contract duration</strong>
            </span>
              <span class="col-md-3">
               <strong>Post date</strong>
            </span>
              <span class="col-md-3">
               <strong>Dead line</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-3">
               <label class="radio-inline"><input type="radio" name="opEmploymentType" checked="" value="Permanent">Permanent</label>
                <label class="radio-inline"><input type="radio" name="opEmploymentType" value="Contract">Contract</label>
            </span>
            <span class="col-md-3">
                <div class="dropdown row">
                  <button class="btn btn-primary dropdown-toggle col-md-12" type="button" data-toggle="dropdown" id="btnDropdown" aria-expanded="false" disabled="">Select
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
                  <ul class="dropdown-menu">
                        <li><a href="http://127.0.0.1:4000/add_vacancy.html#">6 year</a></li>
                        <li><a href="http://127.0.0.1:4000/add_vacancy.html#">1 year</a></li>
                         <li><a href="http://127.0.0.1:4000/add_vacancy.html#">2 years</a></li>
                        <li><a href="http://127.0.0.1:4000/add_vacancy.html#">3 years</a></li>
                      <li><a href="http://127.0.0.1:4000/add_vacancy.html#">4 years</a></li>
                      <li><a href="http://127.0.0.1:4000/add_vacancy.html#">5 years</a></li>
                  </ul>
                </div>

                <!--input type="text" id="txtContractDuration" class="form-control"-->
            </span>
           <span class="col-md-3">
                <input type="text" class="form-control" id="datePostDate">
           </span>
            <span class="col-md-3">
                 <input type="text" id="dateDeadLine" class="form-control">
            </span>
        </div>
        <h3>Company Information</h3>
        <hr>
        <div class="row">
            <span class="col-md-4">
                <strong>Department</strong>
            </span>
            <span class="col-md-4">
                <strong>Location</strong>
            </span>
            <span class="col-md-4">
                <strong>Phone Number</strong>
            </span>
        </div>
        <div class="row">
            <span class="col-md-4">
                    <input type="text" class="form-control" id="txtDepartment">
            </span>
            <span class="col-md-4">
                 <input type="text" value="" class="form-control" id="txtLocation">
            </span>
            
            <span class="col-md-4">
                <input type="text" value="" id="txtPhoneNumber" class="form-control">
            </span>
        </div>
        <div class="row">
             <span class="col-md-3">
                <strong>P.O.Box</strong>
            </span>
            
            <span class="col-md-3">
                <strong>Website</strong>
            </span>
             <span class="col-md-3">
                <strong>Email</strong>
            </span>
             <span class="col-md-3">
                <strong>City</strong>
            </span>
        </div>
        <div class="row">
             <span class="col-md-3">
                <input type="text" id="txtPOBox" class="form-control">
            </span>
            <span class="col-md-3">
                <input type="text" value="" id="txtWebsite" class="form-control">
            </span>
             <span class="col-md-3">
                <input type="text" value="" id="txtEmail" class="form-control">
            </span>
             <span class="col-md-3">
                <div class="tokenfield form-control" style="width: 100%;">
                    <input type="text" id="txtCity" class="form-control" value="Gilgit" tabindex="-1" style="position: absolute; left: -10000px;">
                    <input type="text" tabindex="-1" style="position: absolute; left: -10000px;">
                    <input type="text" class="token-input ui-autocomplete-input" autocomplete="off" placeholder="" id="txtCity-tokenfield" tabindex="0" style="min-width: 60px; width: 116.484px;">
                </div>
            </span>
        </div>
        <div class="row">
            <span class="col-md-9">
                <strong>How to apply</strong>
            </span>
              <span class="col-md-3">
                <strong>Source</strong>
            </span>
        </div>

        <div class="row">
            <span class="col-md-9">
                <label class="radio-inline col-sm-3"><input type="checkbox" name="opHowToApply" value="In Person" checked="">In Person</label>
                <label class="radio-inline col-sm-3"><input type="checkbox" name="opHowToApply" value="Through Mail">Through
                    Mail</label>
                <label class="radio-inline col-sm-3"><input type="checkbox" name="opHowToApply" value="Through Email">Through
                    Email</label>
                <label class="radio-inline"><input type="checkbox" name="opHowToApply" value="Include references">Include
                    references</label>
            </span>
              <span class="col-md-3">
                <div class="tokenfield form-control" style="width: 100%;"><input type="text" class="form-control" id="txtSource" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" tabindex="-1" style="position: absolute; left: -10000px;"><input type="text" class="token-input ui-autocomplete-input" autocomplete="off" placeholder="" id="txtSource-tokenfield" tabindex="0" style="min-width: 60px; width: 224px;"></div>
            </span>
        </div>
        <div class="row">
            <span class="col-md-12">
                <strong>Comments</strong>
                <input type="text" id="txtComment" class="form-control">
            </span>
        </div><br>

        
        <div class="form-group">
        <label>Description:</label>
           
            <!-- <label>Job Description</label> -->
            <textarea class="form-control" rows="5" cols="50" id="txtdescription">

            </textarea>
             
        </div><br>
        <div class="row" style="margin-top: 15px">
            <span class="col-md-2 pull-right">
                <input type="button" class="btn btn-default form-control" value="Send" onclick="javascript:onSendMessage()">
            </span>
        </div>

        

    </form>
</div>


<script type="text/javascript">
    function getSelectedSalary(){
        var salary = "";
        console.log($('#opSalaryAttractive:checked'));

        if ($('#opSalaryAttractive').is(':checked'))
        {
            salary = 'Attractive, ';
        }

        var salaryType = $("input:radio[name=opSalary]:checked").val();
        if (salaryType == 'Fixed')
        {
            salary = salary + $('#txtSalary').val();
        }
        else
        {
            salary = salary  + salaryType;
        }
        console.log('Salary: ' + salary);
        return salary;
    }
    function onSendMessage()
    {
        data                 = {};
        data.title           = $('#txtTitle').val();
        data.qualification   = $('#txtQualification').val();
        data.xfield           = $('#txtField').val();
        data.experience      = 'Range: ' + $('#txtExperience').val() + ', Required: ' + $('#txtExperienceRequired').val();
        data.required_number = $('#txtNumber').val();
        data.additional_req  = $('#txtAdditionReq').val();
        data.description     =$('#txtdescription').val();
        data.salary          = getSelectedSalary(); //$('#txtSalary').val();
        data.job_location    = $('#txtJobLocation').val();
        data.benefits        = $('#txtBenifits').val();
        data.Skills           =$('#txtskills').val();
        data.employment_type = $('input[name=opEmploymentType]:checked', '#myform').val();
        if ($('#btnDropdown').prop('disabled') == false)
        {
            data.contract_duration = $('#btnDropdown').val();
        }
        data.post_date    = $('#datePostDate').val();
        data.dead_line    = $('#dateDeadLine').val();
        data.department   = $('#txtDepartment').val();
        data.location     = $('#txtLocation').val();
        data.phone_number = $('#txtPhoneNumber').val();
        data.po_box       = $('#txtPOBox').val();
        data.email        = $('#txtEmail').val();
        data.city         = $('#txtCity').val();

        var vals = [];
        howToApplySet.forEach(function (key, val, unused)
        {
            vals.push(val);
        });

        data.how_to_apply = 'Interested and qualified applicants should submit their non returnable applications along with CV and photocopies of their relevant documents. ' + (vals).join(', ');
        data.source       = $('#txtSource').val();
        insertNew(data, function(err)
        {
           console.log(err);
        });

    }
    

    
    function toTitleCase(str)
    {
        return str.replace(/\w\S*/g, function (txt)
        {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
    
    function setQualification() {
        $('#txtQualification').tokenfield({
            autocomplete           : {
                source: qualificationList,
                delay : 100
            },
            showAutocompleteOnFocus: true
        });
        $('#txtQualification').on('tokenfield:createtoken', function (event)
        {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function (index, token)
            {
                if (token.value === event.attrs.value)
                {
                    event.preventDefault();
                }
            });
        });
    }
    
    function setExperience() {
        $('#txtExperience').tokenfield({
            autocomplete           : {
                source: experienceList,
                delay : 100
            },
            showAutocompleteOnFocus: true
        });
        $('#txtExperience').on('tokenfield:createtoken', function (event)
        {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function (index, token)
            {
                if (token.value === event.attrs.value)
                {
                    event.preventDefault();
                }
            });
        });
    }
    
    function setField() {
        $('#txtField').tokenfield({
            autocomplete           : {
                source: fieldList,
                delay : 100
            },
            showAutocompleteOnFocus: true
        });
        $('#txtField').on('tokenfield:createtoken', function (event)
        {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function (index, token)
            {
                if (token.value === event.attrs.value)
                {
                    event.preventDefault();
                }
            });
        });
    }

    window.addEventListener('load',function(){
        
        if(typeof($)!=="function"){
            setTimeout(function(){
                console.log("jquery not found");
                this();
            },1000);
        }else{
            console.info("jQuery loaded...");
             initForm();
        }
    });

    function initForm(){

        jQuery(function ($){

 /**
             * Setup for NProgress
             */

       $(document).ajaxStart(function ()
        {
            NProgress.start();
        });

        $(document).ajaxComplete(function ()
        {
            NProgress.done();
        });

         /**
             * Setup for datetime picker
             */

        var defaultStartDate = moment(Date.now());
         
            var defaultStartDate = moment(Date.now());
            var defaultEndDate = moment(Date.now());
            $('#datePostDate').datetimepicker({format: 'DD/MM/YY', defaultDate: defaultStartDate});
            $('#dateDeadLine').datetimepicker({format: 'DD/MM/YY', defaultDate: defaultEndDate});
            
             howToApplySet.add('In Person');

            $(".dropdown-menu li a").click(function ()
            {

                $("#btnDropdown:first-child").html($(this).text() + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span>');
                $("#btnDropdown:first-child").val($(this).text());

            });

            $('#buttonEdit').on('click', function ()
            {
                $('#divAddVacancy').show();
                $('#divDisplay').hide();
            });

            $('#buttonContinue').on('click', function ()
            {
                $('#txtTitle').val('');
                $('#txtQualification').tokenfield('destroy');
                $('#txtQualification').val('');
                $('#txtField').tokenfield('destroy');
                $('#txtField').val('');
                $('#txtExperience').tokenfield('destroy');
                $('#txtExperience').val('');
                $('#txtExperienceRequired').val('');
                $('#txtNumber').val('');
                $('#txtAdditionReq').val('');
                $('#txtdescription').val('');
                $('#txtSalary').val('');
                $('#txtJobLocation').val('');
                $('#txtskills').val('');
                $('#txtBenifits').val('');
                $('input[name=opEmploymentType]:checked', '#myform').val();
                $('#txtContractDuration').val('');
                $('#datePostDate').val();
                $('#dateDeadLine').val();
                $('#txtDepartment').val();
                $('#txtLocation').val();
                $('#txtPhoneNumber').val();
                $('#txtPOBox').val();
                $('#txtEmail').val();
                $('#txtCity').val();
                $('input[name=opHowToApply]:checked', '#myform').val();
                $('#txtSource').val('');

                $('#divAddVacancy').show();
                $('#divDisplay').hide();
                
                setExperience();
                setQualification();
                setField();
            });

            /**
             * Setup for autocomplete
             */
            setExperience();
            setQualification();
            setField();
            
            $('#txtJobLocation').tokenfield({
                autocomplete           : {
                    source: citiList,
                    delay : 100
                },
                showAutocompleteOnFocus: true
            });
            
            $('#txtCity').tokenfield({
                autocomplete           : {
                    source: citiList,
                    delay : 100
                },
                showAutocompleteOnFocus: true
            });
            
            $('#txtSource').tokenfield({
                autocomplete           : {
                    source: sourceList,
                    delay : 100
                },
                showAutocompleteOnFocus: true
            })
            $('#txtSource').on('tokenfield:createtoken', function (event)
            {
                var existingTokens = $(this).tokenfield('getTokens');
                $.each(existingTokens, function (index, token)
                {
                    if (token.value === event.attrs.value)
                    {
                        event.preventDefault();
                    }
                });
            });

            $('#txtCity').on('tokenfield:createtoken', function (event)
            {
                var existingTokens = $(this).tokenfield('getTokens');
                $.each(existingTokens, function (index, token)
                {
                    if (token.value === event.attrs.value)
                    {
                        event.preventDefault();
                    }
                });
            });

            $('#txtJobLocation').on('tokenfield:createtoken', function (event)
            {
                var existingTokens = $(this).tokenfield('getTokens');
                $.each(existingTokens, function (index, token)
                {
                    if (token.value === event.attrs.value)
                    {
                        event.preventDefault();
                    }
                });
            });

            /**
             * setup for contract
             */
            $('#btnDropdown').prop('disabled', true);
            $('#txtSalary').prop('disabled', true);
            $("input[name=opEmploymentType]").change(function ()
            {
                if (this.value == 'Contract')
                {
                    $('#btnDropdown').prop('disabled', false);
                }
                else
                {
                    $('#btnDropdown').prop('disabled', true);
                }
            });

            $('input[name=opHowToApply]').change(function ()
            {
                if (this.checked)
                {
                    howToApplySet.add(this.value);
                }
                else
                {
                    howToApplySet.delete(this.value);
                }
                console.log(howToApplySet);
            });

        });
    }

    var howToApplySet = new Set();

    function onSalary(text)
    {
        if (text == 'fixed')
        {
            $('#txtSalary').prop('disabled', false);
            $('#txtSalary').val('6000000 PKR');
        }
        else
        {
            //$('#txtSalary').val(text);
            $('#txtSalary').prop('disabled', true);
        }
    }
    var experienceList = [];
    experienceList.push('0 year');
    experienceList.push('1-2 years');
    experienceList.push('3-4 years');
    experienceList.push('5-7 years');
    experienceList.push('8-10 years');
    experienceList.push('Above 10 years');

    var qualificationList = [];
    qualificationList.push('PhD');
    qualificationList.push('MSc');
    qualificationList.push('MA');
    qualificationList.push('BSc');
    qualificationList.push('BA');
    qualificationList.push('LLB');
    qualificationList.push('Advanced diploma');
    qualificationList.push('Diploma');
    qualificationList.push('Matric');
    qualificationList.push('FA');
    qualificationList.push('FSc');
    qualificationList.push('Middle');

    var fieldList = [];
    fieldList.push('Accounting AND Finance');
    fieldList.push('Agricultural Engineering');
    fieldList.push('Anesthesiology');
    fieldList.push('Agriculture');
    fieldList.push('Agricultural Economics');
    fieldList.push('Architecture');
    fieldList.push('Automotive Engineering');
    fieldList.push('Auto /General Mechanic');
    fieldList.push('Automotive Technology');
    fieldList.push('Banking And Finance');
    fieldList.push('Biology/Biotechnology');
    fieldList.push('Bio medical engineering');
    fieldList.push('Building Construction');
    fieldList.push('Building/construction  technology');
    fieldList.push('Business Administration/Management');
    fieldList.push('Chemical Engineering');
    fieldList.push('Chemistry');
    fieldList.push('Civil Engineering');
    fieldList.push('Civics and ethical studies');
    fieldList.push('Computer Engineering');
    fieldList.push('Computer Science');
    fieldList.push('Construction management');
    fieldList.push('Cooperative Union');
    fieldList.push('Development And Environment Management');
    fieldList.push('Drafting');
    fieldList.push('Driver');
    fieldList.push('Economics');
    fieldList.push('Education Planning And Management');
    fieldList.push('Electrical And Computer Engineering');
    fieldList.push('Electrical engineering');
    fieldList.push('Electrician /Electronics/Auto electricity');
    fieldList.push('English Language And Literature');
    fieldList.push('Environmental Health');
    fieldList.push('Environmental Protection');
    fieldList.push('Environmental Science');
    fieldList.push('Food production');
    fieldList.push('G.I.S. Expert');
    fieldList.push('Geography');
    fieldList.push('Geology');
    fieldList.push('Governance & Development Study');
    fieldList.push('Health officer');
    fieldList.push('History');
    fieldList.push('Hotel management');
    fieldList.push('Hotel Food Beverage Service');
    fieldList.push('Hotel-Cooking/Food Preparation');
    fieldList.push('Human Resource');
    fieldList.push('Hydraulic/irrigation engineering /hydrology');
    fieldList.push('Industrial Chemistry');
    fieldList.push('Industrial Engineering');
    fieldList.push('Informatics');
    fieldList.push('Information Technology');
    fieldList.push('Journalism And Communication');
    fieldList.push('Kg Teacher');
    fieldList.push('Laboratory Technician');
    fieldList.push('Language Amharic');
    fieldList.push('Legal/Law');
    fieldList.push('Library Science');
    fieldList.push('Linguistics And Literature');
    fieldList.push('Logistics/logistics and supply chain management');
    fieldList.push('Machine technology');
    fieldList.push('Management');
    fieldList.push('Management Information System');
    fieldList.push('Manufacturing');
    fieldList.push('Marketing Management/research');
    fieldList.push('Mathematics');
    fieldList.push('Mechanical Engineering');
    fieldList.push('Medical Doctor');
    fieldList.push('Midwifery');
    fieldList.push('Natural Resource Management');
    fieldList.push('Nursing/ clinical nursing');
    fieldList.push('Office Management');
    fieldList.push('Operator');
    fieldList.push('Pharmacy');
    fieldList.push('Philosophy');
    fieldList.push('Plant science');
    fieldList.push('Political Science And International Relation');
    fieldList.push('Purchasing/procurement and supply management');
    fieldList.push('Psychology/clinical psychology');
    fieldList.push('Public Administration');
    fieldList.push('Public Health');
    fieldList.push('Public Relation');
    fieldList.push('Radiography');
    fieldList.push('Rural Development');
    fieldList.push('Sales and marketing');
    fieldList.push('Sanitary');
    fieldList.push('Secretarial Science');
    fieldList.push('Secretary/Office Assistant');
    fieldList.push('Security Guard');
    fieldList.push('Social Science');
    fieldList.push('Social Work');
    fieldList.push('Sociology/Anthropology');
    fieldList.push('Software Engineering');
    fieldList.push('Soil Science');
    fieldList.push('Special Needs In Education');
    fieldList.push('Sport/Physical Education');
    fieldList.push('Statistics');
    fieldList.push('Supply Management/Material management');
    fieldList.push('Surveying');
    fieldList.push('Textile engineering');
    fieldList.push('Transport Management');
    fieldList.push('Urban Planning');
    fieldList.push('Water supply engineering/water engineering');
    fieldList.push('Wood works/ Related fieldList');

    var citiList = [];
    citiList.push('Abbotabad');
    citiList.push('Astore');
    citiList.push('Bahawalpur');
    citiList.push('Chakwal');
    citiList.push('Chishtian');
    citiList.push('Danyore');
    citiList.push('Diamer');
    citiList.push('Dera Ghazi Khan');
    citiList.push('Gilgit');
    citiList.push('Ghizer');
    citiList.push('Heramosh');
    citiList.push('Hunza');
    citiList.push('Hafizabad');
    citiList.push('Mirpur Khas');
    citiList.push('Quetta');
    citiList.push('Rawalpini');
    citiList.push('Sargodha');
    citiList.push('Sialkot');
    citiList.push('Hyderabad');
    citiList.push('Karachi');
    citiList.push('Lahore');
    citiList.push('Multan');
    citiList.push('Nagar');
    citiList.push('Skardu');
    citiList.push('Shangrilla');
    citiList.push('Shadooor');
    citiList.push('Sadiqabad');
    citiList.push('Faisalabad');
    citiList.push('Islamabad');
    citiList.push('Kohat');
    citiList.push('Peshawar');
    citiList.push('');
    
    



    var sourceList = [];
    sourceList.push('Reporter');
    sourceList.push('FPSC');
    sourceList.push('NTS');
    sourceList.push('Jobs Locator');
    sourceList.push('PTS');



    function insertNew(newVacancy, callback)
        {
            console.log(JSON.stringify(newVacancy));
            $.ajax({
                url        : 'api.php',
                type       : 'POST',
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                data       : 'action=addVacancy&jsondata='+JSON.stringify(newVacancy)
            }).done(function (res)
            {
                console.log(res);
                var vacancy = JSON.parse(res);
                    if(vacancy.status){

                     showMessage('success', vacancy.message);
                     //clear text fields
                    }else{
                     showMessage('error', vacancy.message);
                    }
                callback(null);
            }).fail(function (xhr, statusText, err)
            {
                var text = "";
                console.log(statusText);
                if (xhr.responseText)
                {
                    try
                    {
                        text = (JSON.parse(xhr.responseText)).error.message;
                    }
                    catch (ex)
                    {
                        console.log(ex);
                    }
                }
                showMessage('error', 'Insert vacancy error: ' + text);
                callback(text);
            }).always(function ()
            {

            });
        }

        function showMessage(msgType, msg)
        {
            var n = noty({
                theme   : 'defaultTheme', // or 'relax'
                text    : msg,
                type    : msgType,
                layout  : "topRight",
                modal   : true,
                timeout : 1500,
                template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>'
            });
        }

</script>
