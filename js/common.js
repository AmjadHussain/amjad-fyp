  function showMessage(msgType, msg)
        {
            var n = noty({
                theme   : 'defaultTheme', // or 'relax'
                text    : msg,
                type    : msgType,
                layout  : "topRight",
                modal   : true,
                timeout : 1500,
                template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>'
            });
        }


// item slider
// $(document).ready(function() {
//   $('#media').carousel({
//     pause: true,
//     interval: false,
//   });
// });