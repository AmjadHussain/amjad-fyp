<?php
if(isset($_GET['JID'])){
	$jid =trim($_GET['JID']);
	// echo "we recevied job id $jid";


?>
<?php 
$select="SELECT * FROM vacancy WHERE PID=".	$jid;
$qry=mysqli_query($conn,$select);
// var_dump($qry);
$message=array();
$vacancy=mysqli_fetch_assoc($qry);
if ( $vacancy) {
	

	// echo "<pre>";
	// print_r($_POST);
	// echo "</pre>";

	if(isset($_SESSION['details']) && isset($_POST['action']) ){
		if($_POST['action']=="apply"){
			$xuid = mysqli_escape_string($conn,$_SESSION['details']['UID']);
			$xjid = mysqli_escape_string($conn,$_POST['JID']);


			$applieddata = mysqli_query($conn,"SELECT * FROM myapplications WHERE UID={$xuid} AND PID={$xjid}");
			if(mysqli_num_rows($applieddata)>0){
				$message['type']="default";
				$message['message']= "Already Applied for the Job";
			}else{
				$result =mysqli_query($conn,"INSERT into myapplications (UID,PID) VALUES({$xuid},{$xjid})");
				if(mysqli_insert_id($conn)>0){
					$message['type']="success";
					$message['message']="Applied";
				}else{
					$message['type']="error";
					$message['message']=  mysqli_error($conn);
				}
			}
		}	
	}


 ?>
<div class="container">
	<div class="row">
			<?php
				if(count($message)>0): ?>
				<script src="<?php echo $BASE_URL; ?>/js/common.js"></script>
				<script>
				window.addEventListener('load',function(){
					showMessage('<?php echo $message['type']; ?>','<?php echo $message['message']; ?>');
				});
				</script>
			<?php	
			endif;
			?>
    
         <div class="col-sm-12">
         <h1  href="#" class="list-group-item active text-center"> Job Description</h1>
        <span class="list-group-item"><b>Job Title :</b><?php echo $vacancy['title']; ?> </span>
        <span class="list-group-item"><b>Description :</b><?php echo $vacancy['description']; ?> </span>
        <span class="list-group-item"><b>Skills :</b><?php echo $vacancy['skills']; ?> </span>

				<div class="table-responsive">				  
				   <table class="table table-bordered">
				   <thead>
				      <tr>
				         <th>Vacancies</th>
				         <th>Salary</th>
				         <th>Qualification</th>
				         <th>field</th>
				         <th>Emp Type</th>
				         <th> Location</th>
				         <th> City</th>
				         <th>Email</th>
				         <th>Experience</th>
				         <th>Experience</th>

				      </tr>
				   </thead>
				   <tbody>
				      <tr>
				         <td><?php echo $vacancy['required_number']; ?></td>
				         <td><?php echo $vacancy['salary']; ?></td>
				         <td><?php echo $vacancy['qualification']; ?></td>
				         <td><?php echo $vacancy['xfield']; ?></td>
				         <td><?php echo $vacancy['employment_type']; ?></td>
				         <td><?php echo $vacancy['location']; ?></td>
				         <td><?php echo $vacancy['city']; ?></td>
				         <td><?php echo $vacancy['email']; ?></td>
				         <td><?php echo $vacancy['experience']; ?></td>

				         <td></td>
				      </tr>
				   </tbody>
				</table>
                </div>

                <div class="table-responsive">				  
				   <table class="table table-bordered">
				   <thead>
				      <tr>
				         <th>Job Location</th>
				         <th>Department</th>
				         <th>P.O.Box</th>
				         <th>Contract Duration</th>
				         <th> Benefits</th>
				         <th> Post Date</th>
				        </tr>
				   </thead>
				   <tbody>
				      <tr>
				         <td><?php echo $vacancy['job_location']; ?></td>
				         <td><?php echo $vacancy['department']; ?></td>
				         <td><?php echo $vacancy['po_box']; ?></td>
				         <td><?php echo $vacancy['contract_duration']; ?></td>
				         <td><?php echo $vacancy['benefits']; ?></td>
				         <td><?php echo $vacancy['post_date']; ?></td>
				        
				         

				      </tr>
				   </tbody>
				</table>
                </div>

        <span class="list-group-item"><b>Addtional Requirement :</b><?php echo $vacancy['additional_req']; ?> </span>
        <span class="list-group-item"><b>Last Date :</b><?php echo $vacancy['dead_line']; ?> </span>
        <span class="list-group-item"> <b>How to Apply :</b><?php echo $vacancy['how_to_apply']; ?> </span>
        <span class="list-group-item"><b>Source :</b><?php echo $vacancy['source']; ?>

         <form method="POST">
         	<input type='hidden' name="action" value="apply"/>
         	<input type='hidden' name="JID" value="<?php echo $_GET['JID']; ?>"/>
        	<button type="submit"  class="btn btn-success apply_btn pull-right"> Apply Now</button>
        </span>
       	<span class="clearfix"></span>
        </form>
        </div>
        <div class="clearfix"></div>
        
       
   </div>
</div>

<?php 
}
} ?>