<?php
if(!isset($BASE_PATH)) die("<b>Access Denied</b>");
?>
<div class="container">
	<div class="row">
		<div class="col-sm-4  col-sm-offset-4 " style="margin-top:13%; margin-bottom:10%;">
	<form method="POST" action="<?php echo $BASE_URL;
?>/Signup.php">
		<?php
			if(isset($_GET['message'])){
	echo "<b style='color:red;'>".str_replace("+","",$_GET['message'])."</b>";
}
?>
		<fieldset>
			<legend>Register Yourself</legend>
			<label class="" for="username">Username </label><br>

			<input class="form-control"  type="text" name="username" placeholder="Please enter your name"   maxlength="30" 
            required="" /><br>
			
			<label class=""  for="password">Password </label><br>
			<input class="form-control"  type="password" name="password" id="password" type="password" id="confirm_password" pattern=".{6,}" 
            required title="Password should be Six or more characters" placeholder="Password should be Six characters long" maxlength="20"required="" /><br>
			
			<label class=""  for="password">Confirm Password </label><br>
			<input class="form-control"  type="password" id="confirm_password" pattern=".{6,}" 
            required title="Password should be Six or more characters" placeholder="Password should be Six characters long" maxlength="20"required="" /><br>
			
			<label class=""  for="email">Email </label><br>
            <input class="form-control"  type="email" name="email" pattern=".[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
              required title="Email should only contain lowercase and upwercase letters and numbers. e.g.amju525@gmail.com" maxlength="40" 
             placeholder="example@gmail.com" required="" /><br>

			<label class=""  for="height">Phone </label><br>
			<input class="form-control"  type="text" name="phone"pattern="^(\+[0-9]{1,5})?([1-9][0-9]{9})$" placeholder="+92312xxxxxx" 
           required title="Please enter your phone number  e.g +92312xxxxxxx"  maxlength="18" required="" /><br>
		    		   <div>
            <label class=""  for="profile">Select Your Role </label><br>
                  <select required="" name="profile" class="form-control">
                   
                     <option value="Job_seeker"> Seeker</option>
                     <option value="Job_provider"> Organization</option>

                  </select>
               </div> </br>

			<input class="form-control btn btn-success" name="register" type="submit" value="Register"/>
		</fieldset>
	</form>
	</div>
	</div>
</div>

<script>
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;	

</script>