<?php require_once 'includes/config.php'; ?>
<div class="panel panel-info">
      <div class="panel-heading">
         <h4 class=" text-center panel-title">
            <a data-toggle="collapse" data-parent="#accordion" 
               href="#collapseThree">
              Click here to Show the Searching Results
            </a>
         </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse">
         <div class="panel-body">
          
         <?php


if (!empty($_REQUEST['term'])) {
 // $jobsresult = mysqli_query($conn,"SELECT V.*,C.*,M.image from vacancy as V,users as C,store as M WHERE V.UID=C.UID  AND C.UID=M.UID ORDER BY V.PID $sort LIMIT {$offset},{$perpage}");

$term = mysqli_real_escape_string($conn,$_REQUEST['term']);     

$sql = "SELECT V.*, U.* FROM vacancy as V ,users as U WHERE U.username OR V.skills LIKE '%".$term."%'"; 
$r_query = mysqli_query($conn,$sql); 

while ($row = mysqli_fetch_assoc($r_query)){  ?>
<div class="panel panel-default">
        <div class="panel-heading">
        <div class="row">
          <div class="col-xs-8">
            <a href="<?php echo $BASE_URL; ?>?page=viewjob&JID=<?php echo $job['PID']; ?>">
              <h3 style="margin:0px;">
                <?php echo $row['title']; ?>
              </h3>
            </a>
          </div>
        </div>
          
        </div>
          <div class="panel-body">
            <p>
              <?php echo $row['description']; ?> 
            </p>
          </div>
            <div class="panel-footer">
              <div class="user pull-left">
                      <div class="avatar pull-left" style="height:25px; width:25px;" >
                        <img src="<?php echo $row['image'] ; ?>" class="img-circle img-responsive" style="width:100%;height:100%;border-radius:50%;background-color:red;"/>
                      </div>
                      <div class="details pull-left" style="padding:5px; margin-left:10px">
                        <a href="<?php echo $BASE_URL; ?>?page=publicprofile&cid=<?php echo $job['UID']; ?>">
                          <strong><?php echo $job['username'];?> </strong>
                        </a>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              <a href="<?php echo $BASE_URL; ?>?page=viewjob&JID=<?php echo $job['PID']; ?>" class=" pull-right btn btn-primary">
               View
              </a>
              <div class="clearfix"></div>
          </div>
      </div>  
  
<?php }
 }
else{
  echo "Enter some text in the form";

}
 ?>  

         </div>
      </div>
   </div>
   <script type="text/javascript">
 
 
   $(function () { $('#collapseThree').collapse('toggle')});

</script>