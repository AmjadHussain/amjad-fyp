<div class="jumbotron">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <strong>
        <h2 class=" search_title">Search Your required Job</h2>
        </strong>
        <form class="form-inline form-layout" method="GET" >
        <!-- <form class="form-inline form-layout" method="POST" action="<?php echo $BASE_URL; ?>?page=search"> -->
            <div class="form-group skills_form">
              <input type="text" class="col-sm-4 form-control skills_form" name="keyword" id="skills" 
               placeholder="Enter your skills">
            </div>
            <div class="form-group">
              <select class=" col-sm-4 skills_form form-control" id="search-category" name="category">
              </select>
            </div>
           
            <!-- <button type="submit"  class="btn btn-success">Search</button> -->
             <button type="submit"  class="btn btn-success">Search</button>
        </form>
        <br><br>
      </div>
    
    </div>
  </div>
</div>
<!-- slider -->
<!-- <div class="jumbotron jb">
<div class="container">

  <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide media-carousel" id="media">
        <div class="carousel-inner">
          <div class="item  active">
            <div class="row">
              <div class="col-md-12">
                <a class="thumbnail" href="#"><img alt="" src="http://i2.wp.com/www.montanaoutdoor.com/wp-content/uploads/2012/11/MLB_MToutdoor_WEB_600x100_final-1.jpg?resize=600%2C100"></a>
              </div>         
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-12">
                <a class="thumbnail" href="#"><img alt="" src="http://i2.wp.com/www.montanaoutdoor.com/wp-content/uploads/2012/11/BlacktailBanner.jpg?resize=600%2C100"></a>
              </div>         
            </div>
          </div>
        
           
        </div>
        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
        <a data-slide="next" href="#media" class="right carousel-control">›</a>
      </div>                          
    </div>
  </div>
</div>
</div> -->
      
      
    

   

<!-- post area starts from here -->
<div class="container" >
  <div class="row" >
    <div class="col-sm-8" >
    <div class="panel latest_heading latest_posts" ><strong class="text-center"><h4>Latest Jobs Posted by Companies</h4></strong> </div>
      
    <?php
      $jobsperpage=10;
      if(isset($_GET['keyword']) && isset($_GET['category'])){
        getSearchJobs(mysqli_escape_string($conn,$_GET['keyword']),mysqli_escape_string($conn,$_GET['category']),$jobsperpage);
        echo "<div class='text-center'>";
        generateSearchJobPagination(mysqli_escape_string($conn,$_GET['keyword']),mysqli_escape_string($conn,$_GET['category']),$jobsperpage,"&page=home&category=".$_GET['category']."&keyword=".$_GET['keyword']);
        echo "</div>";
      }else{
          getLatestJobs($jobsperpage);
          echo "<div class='text-center'>";
          generateLatestJobPagination($jobsperpage,"&page=home");
          echo "</div>";
     }
    ?>

 </div>
  <!-- Aside Latest Post portion -->
   
     <div class="col-sm-4 ">
      <!-- <h2 class="text-center"> Top Job Providers</h2> -->

       <?php require_once 'top_job_providers.php'; ?>
     </div>
  </div>
</div>
<!-- Aside Latest Post portion ends here -->



 <script type="text/javascript">
  window.addEventListener('load',function(){

    searchCategory = document.querySelector('#search-category');
    var all = document.createElement('option');
    all.value = "";
    all.innerHTML = "All";
    searchCategory.appendChild(all);

    for(i=0;i<fieldList.length;i++){
      var option = document.createElement('option');
      option.setAttribute('value',fieldList[i]);
      option.innerHTML=fieldList[i];
      searchCategory.appendChild(option);
    }
  });
</script>