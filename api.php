<?php 	
require_once 'includes/config.php';
$response = array('status' => false,'message' =>'Invalid request param missing'.print_r($_POST,true));



if(isset($_POST['action'])){
	if($_POST['action']=="addVacancy"){
		$vacancy= json_decode($_POST['jsondata'],TRUE);
		$vacancy['UID']=$_SESSION['details']['UID'];

		$details_queryery = "INSERT into vacancy SET ";
		$columns = array_keys($vacancy);
		$data=array();
		for($i=0;$i<count($vacancy);$i++){
			$data[$columns[$i]]=mysqli_escape_string($conn,trim($vacancy[$columns[$i]]));
			if($i>0)
				$details_queryery.=",";
			if(strlen($columns[$i]) > 0)
				$details_queryery.="  {$columns[$i]} = '".mysqli_escape_string($conn,trim($vacancy[$columns[$i]]))."' ";
		}

		if($conn){
			$result=mysqli_query($conn,$details_queryery);
			$PID=mysqli_insert_id($conn);
			if($PID>0){

				//1-get all related users
				//send message using curl.

				$query = "SELECT U.UID,U.fcm_token from users as U,user_details as UD,vacancy as V ";
				$query .=" WHERE V.PID ={$PID} AND UD.UID=U.UID AND (V.qualification = UD.option_value AND ";
				$query .=" UD.option_key = 'qualification') OR (V.xfield = UD.option_value AND UD.option_key = 'xfield') ";
				$query .=" OR (V.experience = UD.option_value AND UD.option_key = 'experience') AND U.fcm_token !='' GROUP BY U.UID";

				$users_results = mysqli_query($conn,$query);

				$response['curlresponse']=array(); 
				$response['matched_users'] =mysqli_num_rows($users_results);
				if(mysqli_num_rows($users_results)>0){
					$response['fcm_status'] = array();
					while(($row = mysqli_fetch_assoc($users_results)) != null){
						if(strlen($row['fcm_token'])>100)
							if(sendNotification($row['UID'],$PID,$row['fcm_token'])){
								$response['fcm_status'] = "Message send to {$row['UID']}  {$row['UID']},{$PID},{$row['fcm_token']}";
							}
					}
				}


				$response['status']=$data;
				$response['status']=true;
				$response['message']="Vacancy added successfully...";
			}else{
				$response['status']=true;
				$response['message']="Failed to add new vacancy";
				$response['dbmessage']=mysqli_error($conn);
				$response['query']=$details_queryery;
			}
		}else{
			$response['message']="Failed to place query.DB error contact admin";
		}
	}

	else if($_POST['action']=="updateVacancy"){
		$vacancy= json_decode($_POST['jsondata'],TRUE);
		$vacancy['UID']=$_SESSION['details']['UID'];

		$details_queryery = "UPDATE vacancy SET ";
		$columns = array_keys($vacancy);
		for($i=0;$i<count($vacancy);$i++){
			if($i>0)
				$details_queryery.=",";
			if(strlen($columns[$i]) > 0)
				$details_queryery.="  {$columns[$i]} = '".mysqli_escape_string($conn,trim($vacancy[$columns[$i]]))."' ";
		}
		$details_queryery.=" WHERE PID={$_POST['JID']}";

		if($conn){
			$result=mysqli_query($conn,$details_queryery);
			if(mysqli_insert_id($conn)>0){
				$response['status']=true;
				$response['message']="Vacancy added successfully...";
			}else{
				$response['status']=true;
				$response['message']="Updated Sucessfully";
				$response['dbmessage']=mysqli_error($conn);
				$response['query']=$details_queryery;
			}
		}else{
			$response['message']="Failed to place query.DB error contact admin";
		}
	}else if($_POST['action']=="register"){

		if(isset($_POST['email']) && isset($_POST['password'])
			 && isset($_POST['qualification']) && isset($_POST['experience'])
			 	&& isset($_POST['field']) && isset($_POST['username'])
			 		 && isset($_POST['phone'])){


			$verify=mysqli_query($conn,"SELECT * from users WHERE username='".mysqli_escape_string($conn,$_POST['username'])."' OR email='".mysqli_escape_string($conn,$_POST['email'])."'");

			if(mysqli_num_rows($verify)){
				$response['message'] = "Username or Email already registered";
			}else{
				$query = 'INSERT INTO users (username,password,email,phone,profile) VALUES (
							"'.mysqli_escape_string($conn,$_POST['username']).'",
							"'.mysqli_escape_string($conn,$_POST['password']).'",
							"'.mysqli_escape_string($conn,$_POST['email']).'",
							"'.mysqli_escape_string($conn,$_POST['phone']).'","Job_seeker")';

				$status = mysqli_query($conn,$query);

				if($status && mysqli_insert_id($conn)>0){
					$UID =mysqli_insert_id($conn);
					$details_query = "INSERT INTO user_details (UID, option_key, option_value) VALUES ";
					$details_query .="($UID,'address',''),";
					$details_query .="($UID,'gender',''),";
					$details_query .="($UID,'dob',''),";
					$details_query .="($UID,'city',''),";
					$details_query .="($UID,'country',''),";
					$details_query .="($UID,'experience','".mysqli_escape_string($conn,$_POST['experience'])."'),";
					$details_query .="($UID,'skills',''),";
					$details_query .="($UID,'xfield','".mysqli_escape_string($conn,$_POST['field'])."'),";
					$details_query .="($UID,'qualification','".mysqli_escape_string($conn,$_POST['qualification'])."')";

					$status =mysqli_query($conn,$details_query);
					if($status && mysqli_insert_id($conn)>0){
						$response['status'] = true;
						$response['UID'] = $UID;
						$response['message'] = "successfully Registered";
					}
				}else{
					$response['dberror'] = mysqli_error($conn);
				}
			}

		}

	}else if($_POST['action']=="profileupdate"){

		if(isset($_POST['email']) && isset($_POST['password'])
			 && isset($_POST['qualification']) && isset($_POST['experience'])
			 	&& isset($_POST['field']) && isset($_POST['username'])
			 		 && isset($_POST['phone'])){


			$verify=mysqli_query($conn,"SELECT * from users WHERE  email='".mysqli_escape_string($conn,$_POST['email'])."' AND UID != ".mysqli_escape_string($conn,$_POST['UID']));

			if(mysqli_num_rows($verify)){
				$response['message'] = "Email already Exists";
			}else{
				$query = 'UPDATE users SET email="'.mysqli_escape_string($conn,$_POST['email']).'"';
				$query .= ',phone="'.mysqli_escape_string($conn,$_POST['phone']).'"';

				if(trim($_POST['password'])!="********")
					$query .= ',password="'.mysqli_escape_string($conn,$_POST['password']).'"';

				$query .= ' WHERE UID='.mysqli_escape_string($conn,$_POST['UID']);

				$status = mysqli_query($conn,$query);

				if($status){
					$UID =mysqli_insert_id($conn);
					$details_query = "UPDATE user_details SET option_value='".mysqli_escape_string($conn,$_POST['experience'])."' WHERE UID=".mysqli_escape_string($conn,$_POST['UID'])." AND option_key='experience'";
					$status =mysqli_query($conn,$details_query);
					$response['error']=array();
					if(!$status){
						$response['error']['experience']= mysqli_error($conn);
					}


					$details_query = "UPDATE user_details SET option_value='".mysqli_escape_string($conn,$_POST['field'])."' WHERE UID=".mysqli_escape_string($conn,$_POST['UID'])." AND option_key='xfield'";
					$status =mysqli_query($conn,$details_query);
					if(!$status){
						$response['error']['experience']= mysqli_error($conn);
					}


					$details_query = "UPDATE user_details SET option_value='".mysqli_escape_string($conn,$_POST['qualification'])."' WHERE UID=".mysqli_escape_string($conn,$_POST['UID'])." AND option_key='qualification'";
					$status =mysqli_query($conn,$details_query);
					if(!$status){
						$response['error']['experience']= mysqli_error($conn);
					}
					$response['status'] =true;
					$response['UID'] =$_POST['UID'];
					$response['message'] = "successfully Registered";
				}else{
					$response['dberror'] = mysqli_error($conn);
				}
			}

		}

	}else if($_POST['action']=="getquestions"){
		$offset=0;
		$perpage=10;
		$sort = "DESC";


		if(isset($_POST['offset']))
			$offset = intval($_POST['offset']);

		if(isset($_POST['perpage']))
			$perpage = intval($_POST['perpage']);

		if(isset($_POST['sort']))
			$sort = mysqli_escape_string($conn,$_POST['sort']);
		

		$jobsresult = mysqli_query($conn,"SELECT V.*,C.*,M.image from vacancy as V,users as C,store as M WHERE V.UID=C.UID  AND C.UID=M.UID ORDER BY V.PID $sort LIMIT {$offset},{$perpage}");

		if(mysqli_num_rows($jobsresult)>0){
			$dataset =array();
			while( ($row =mysqli_fetch_assoc($jobsresult)) !=null){
				$dataset[]=$row;
			}

			$response['status'] = true;
			$response['message'] = "Total ".count($dataset)." jobs found";
			$response['data'] = $dataset;
		}else{
			$response['message']="Sorry No Jobs";
		}

	}else if($_POST['action']=="applyjob"){

		if(isset($_POST['PID']) && isset($_POST['UID'])){

			$PID= mysqli_escape_string($conn,$_POST['PID']);
			$UID= mysqli_escape_string($conn,$_POST['UID']);

			$query = "SELECT * from myapplications WHERE UID={$UID} AND PID={$PID}";
			$result = mysqli_query($conn,$query);

			if($result && mysqli_num_rows($result)>0){
				$response['message']="اپ کی در خواست پحلے ھی پحنچ چکی ھے";
			}else{	
				$query ="INSERT INTO myapplications (UID,PID) VALUES ({$UID},{$PID})";
				$result = mysqli_query($conn,$query);
				if($result && mysqli_insert_id($conn)>0){
					$response['status'] = true;
					$response['message'] = "Congratulations! You application has been submitted";
				}else{
					$response['dberror'] = mysqli_error($conn);
				}


			}

		}

	}else if($_POST['action']=="auth"){

		if(isset($_POST['username']) && isset($_POST['password'])){

			$username= mysqli_escape_string($conn,$_POST['username']);
			$password= mysqli_escape_string($conn,$_POST['password']);

			$query = "SELECT UID,username,email,phone from users WHERE (username='{$username}' OR email='{$username}') AND password='{$password}' AND profile='Job_seeker' LIMIT 1";
			$result = mysqli_query($conn,$query);

			if($result && mysqli_num_rows($result)>0){
				$response['status'] = true;
				$response['message'] = "Welcome Again!";
				$data=mysqli_fetch_assoc($result);
				
				$UID=$data['UID'];

				$query = "SELECT * from user_details WHERE UID={$UID}";
				$result = mysqli_query($conn,$query);
				if($result && mysqli_num_rows($result)>0){
					while(($row = mysqli_fetch_assoc($result)) != null){
						$data[$row['option_key']] = $row['option_value'];	
					}
				}
				$response['data'] = $data;

			}else{	
				$response['dberror'] = mysqli_error($conn);
				$response['message'] = "Invalid Username or Password";
			}

		}

	}else if($_POST['action']=="userdetails"){

		if(isset($_POST['UID'])){
			
			$UID= mysqli_escape_string($conn,$_POST['UID']);

			$query = "SELECT * from users WHERE UID={$UID}";
			$result = mysqli_query($conn,$query);

			if($result && mysqli_num_rows($result)>0){
				$response['status'] = true;
				$response['message'] = "Welcome Again!";
				$response['user'] = mysqli_fetch_assoc($result);

				$query = "SELECT * from user_details WHERE UID={$UID}";
				$details_result = mysqli_query($conn,$query);

				if($details_result && mysqli_num_rows($details_result)>0){
					$details=array();
					while (($row=mysqli_fetch_assoc($details_result)) != null) {
							$details[$row['option_key']]=$row['option_value'];
					}
					$response['details']=$details;
				}else{
					$response['details']=array();
					if(mysqli_num_rows($details_result)==0){
						$response['result']="Not Details found";
					}else{
						$response['dberror'] = "Something went wrong \n". mysqli_error($conn);
					}
				}
			}else{	
				$response['dberror'] = mysqli_error($conn);
				$response['message'] = "Invalid Username or Password";
			}
		}
	}else if($_POST['action']=="updatetoken"){
		if( isset($_POST['token']) && isset($_POST['UID'])){
			$UID = mysqli_escape_string($conn,$_POST['UID']);
			$token = mysqli_escape_string($conn,$_POST['token']);

			$result = mysqli_query($conn, "UPDATE users SET fcm_token = '{$token}' WHERE UID = {$UID}");

			if($result){
				$response['status'] = true;
				$response['message'] = 'FCM Token Updated Successfully';
 			}else{
 				$response['message'] = 'FCM Token update failed';
 				$response['dberror'] = mysqli_error($conn);
 			}
		}
	}else if($_POST['action']=="myapplications"){
		if( isset($_POST['UID'])){

			$UID = mysqli_escape_string($conn,$_POST['UID']);

			$jobsresult = mysqli_query($conn,"SELECT V.*,C.*,M.image from vacancy as V,users as C,store as M,myapplications as MA WHERE V.UID=C.UID  AND C.UID=M.UID AND MA.PID=V.PID AND MA.UID={$UID}");

			if(mysqli_num_rows($jobsresult)>0){
				$dataset =array();
				while( ($row =mysqli_fetch_assoc($jobsresult)) !=null){
					$dataset[]=$row;
				}

				$response['status'] = true;
				$response['message'] = "Total ".count($dataset)." jobs found";
				$response['data'] = $dataset;
			}else{
				$response['message']="Sorry No Applications";
			}

		}
	}else if($_POST['action']=="mynotifications"){
		if( isset($_POST['UID'])){

			$UID = mysqli_escape_string($conn,$_POST['UID']);
				


			$jobsresult = mysqli_query($conn,"SELECT  * from messages WHERE UID={$UID} ORDER BY MID DESC");

			if(mysqli_num_rows($jobsresult)>0){
				$dataset =array();
				while( ($row =mysqli_fetch_assoc($jobsresult)) !=null){
					$dataset[]=$row;
				}

				$response['status'] = true;
				$response['message'] = "Total ".count($dataset)." Notifications found";
				$response['data'] = $dataset;
			}else{
				$response['message']="Sorry No Notifications";
			}

		}
	}else if($_POST['action']=="addnotification"){
		if( isset($_POST['UID']) && isset($_POST['PID']) && isset($_POST['token'])){

			$UID = mysqli_escape_string($conn,$_POST['UID']);
			$PID = mysqli_escape_string($conn,$_POST['PID']);
			if(sendNotification($UID,$PID,$_POST['token'])){
					
					$response['status'] = true;
					$response['message'] = "Notification Added";
			}
		}
	}
}

function sendNotification($UID,$PID,$token){
	global $conn,$response,$FCM_API_KEY;
	$result = mysqli_query($conn,"SELECT * from vacancy WHERE PID={$PID}");
	if(mysqli_num_rows($result)>0){
		$vacancy = mysqli_fetch_assoc($result);

		$message =array('JID'=>$PID,'message'=>"<b>{$vacancy['title']}<b><p>{$vacancy['description']}</p>");

		$json = json_encode($message);

		$result = mysqli_query($conn , "INSERT INTO messages (UID,message) VALUES('{$UID}','{$json}')");
		if(mysqli_insert_id($conn)>0){

			$headers = array();
			$headers[] = 'Content-Type:application/json';
			$headers[] = 'Authorization: key=' . $FCM_API_KEY;


			$body = array('to' => $token,
						 'data' => array('title' => $vacancy['title'] ,'description' =>$vacancy['description']));

			$response['curlresponse'][]=sendFCM($headers,$body);
			return true;

		}else{
			$response['message']="Sorry No Notifications";
			return false;
		}

	}
}
 

echo json_encode($response);
?>