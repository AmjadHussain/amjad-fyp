<!DOCTYPE html>
<html>
<head>
	<title> JobsLocator</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/index.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/bootstrap-tokenfield.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/nprogress.css">
     <!-- <link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/custom.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo $BASE_URL ; ?>/css/bootstrap-datetimepicker.min.css">

</head>
<body>