<?php


function getLatestJobs($perpage=10,$sort="DESC"){
  global $conn;
  global $BASE_URL;

  $offset=0;
  if(isset($_GET['offset'])){
    $offset = $perpage*intval($_GET['offset']);
  }

if($conn){

    $jobsresult = mysqli_query($conn,"SELECT V.*,C.*,M.image from vacancy as V,users as C,store as M WHERE V.UID=C.UID  AND C.UID=M.UID ORDER BY V.PID $sort LIMIT {$offset},{$perpage}");
    if($jobsresult){
      while(($job=mysqli_fetch_assoc($jobsresult)) != null): ?>
     <div class="container">

                 <div class="col-sm-8 blogShort">
                   <h4 class="des"> <b>&nbsp&nbsp&nbsp Job Title :</b><?php echo $job['title']; ?></h4> 
                     <img src="<?php echo $job['image'] ; ?>" alt="post img" class="pull-left img-responsive img-thumbnail" style="height:100px; width:100px; background-color:#f15f43;" >
                     <br><br>
                         <em class="des">&nbsp&nbsp&nbsp Published by: <a href="<?php echo $BASE_URL; ?>?page=publicprofile&cid=<?php echo $job['UID']; ?>">
                          <strong><?php echo $job['username'];?> </strong>
                        </a></em>
                        <br><br>
                     <div class="col-sm-10"><p class=" des">
                         <?php echo $job['description']; ?>   
                         </p>
                            
                         
                     <a class="btn btn-blog pull-right marginBottom10"href="<?php echo $BASE_URL; ?>?page=viewjob&JID=<?php echo $job['PID']; ?>"> View & Apply</a> 
                 </div>
                 </div>
                  
                 
                 
               <div class="col-md-12 margin10"></div>
             </div>


          <?php
      endwhile; 
    }else{
      echo mysqli_error($conn);
      ?>

      <b>No Jobs Updates</b>
      <?php
    }



  }else{ ?>

  <b>Database Connection Error!</b>
  
  <?php
  }


}
// echo "working";



function generateLatestJobPagination($perpage=10,$query_params){
  global $conn;
  echo "<ul class='pagination'>";
  $jobsresult = mysqli_query($conn,"SELECT count(V.PID) as totaljobs from vacancy as V LIMIT {$perpage}");
  if($jobsresult){
    $row = mysqli_fetch_assoc($jobsresult);
    $totaljobs = $row['totaljobs'];
    $pages=ceil($totaljobs/$perpage);
    for($i=0;$i<$pages;$i++){
      if($i==0)
      echo "<li><a href='?offset=0{$query_params}'>Home</a></li>";
      else      
      echo "<li><a href='?offset={$i}{$query_params}'>{$i}</a></li>";      
    }
  }else{
    echo mysqli_error($conn);
  }
  echo "</ul>";
}

function generateSearchJobPagination($keyword,$category,$perpage=10,$query_params){
  global $conn;
  $search_where = " (V.title LIKE '%$keyword%' AND V.xfield LIKE '%$category%') AND ";
  $query = "SELECT count(V.PID) as totaljobs from vacancy as V,users as C,store as M WHERE $search_where V.UID=C.UID  AND C.UID=M.UID";
  
  file_put_contents("page_raw_query.log", $query);
  echo "<ul class='pagination'>";
  $jobsresult = mysqli_query($conn,$query);
  $totaljobs=0;
  if($jobsresult){
    $row = mysqli_fetch_assoc($jobsresult);
    $totaljobs = $row['totaljobs'];
    $pages=ceil($totaljobs/$perpage);
    for($i=0;$i<$pages;$i++){
      if($i==0)
      echo "<li><a href='?offset=0{$query_params}'>Home</a></li>";
      else      
      echo "<li><a href='?offset={$i}{$query_params}'>{$i}</a></li>";      
    }
  }else{
    echo mysqli_error($conn);
  }
  echo "</ul> <div class='text-center'><b>TOTAL RESULTS: {$totaljobs}</b></div>";
}




function getSearchJobs($keyword,$category,$perpage=10,$sort="DESC"){
  global $conn;
  global $BASE_URL;

  $offset=0;
  if(isset($_GET['offset'])){
    $offset = $perpage*intval($_GET['offset']);
  }

  $search_where = " (V.title LIKE '%$keyword%' AND V.xfield LIKE '%$category%') AND ";


  if($conn){
    $query = "SELECT V.*,C.*,M.image from vacancy as V,users as C,store as M WHERE $search_where V.UID=C.UID  AND C.UID=M.UID ORDER BY V.PID $sort LIMIT {$offset},{$perpage}";
    file_put_contents("raw_query.log", $query);
    $jobsresult = mysqli_query($conn,$query);
    if($jobsresult){
      while(($job=mysqli_fetch_assoc($jobsresult)) != null): ?>
       <div class="container">

                 <div class="col-md-8 blogShort">
                   <h4> <b>Job Title :</b><?php echo $job['title']; ?></h4> 
                     <img src="<?php echo $job['image'] ; ?>" alt="post img" class="pull-left img-responsive img-thumbnail" style="height:100px; width:100px; background-color:#f15f43;" >
                     
                         <em> Publishe by: <a href="<?php echo $BASE_URL; ?>?page=publicprofile&cid=<?php echo $job['UID']; ?>">
                          <strong><?php echo $job['username'];?> </strong>
                        </a></em>
                     <article><p>
                         <?php echo $job['description']; ?>   
                         </p>
                             <p></p>
                         </article>
                     <a class="btn btn-blog pull-right marginBottom10"href="<?php echo $BASE_URL; ?>?page=viewjob&JID=<?php echo $job['PID']; ?>"> View & Apply</a> 
                 </div>
                  
                 
                 
               <div class="col-md-12 margin10"></div>
             </div>

          <?php
      endwhile; 
    }else{
      echo mysqli_error($conn);
      ?>

      <b>No Jobs Updates</b>
      <?php
    }



  }else{ ?>

  <b>Database Connection Error!</b>
  
  <?php
  }


}


?>